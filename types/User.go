package types

type User struct {
	Id        string
	Username  string
	Avatar    string
	CreatedAt string
}
